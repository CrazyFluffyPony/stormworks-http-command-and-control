var express = require('express');
var router = express.Router();

let messages = []

router.get('*', async function(req, res, next) {
    res.send('received a GET')


	let qstring = req.originalUrl.substring(req.baseUrl.length+1)	

	let resp = qstring.substring('?resp='.length)

	if(resp === 'finished'){
		console.log(messages.join(''))
	} else {

		let buff = new Buffer(resp, 'base64');
		let text = buff.toString('ascii');

		messages.push(text)
	}
})

router.post('/', async function(req, res, next) {
    res.send('received a POST')
	console.log(req.body)
})


module.exports = router
